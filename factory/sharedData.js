define("sharedData", ["jquery"], function($) {
  	
	
	var firebaseRoot = "sizzling-fire-8858";
	var firebaseIP  = "https://" + firebaseRoot + ".firebaseio.com";	
	
	
	var sharedDataObj = {}; 
        //sharedDataObj["logState"] = localStorage.getItem("logState");
        phpJS.getPHPStatus(function(state, data){
        	sharedDataObj["phpStatus"] = JSON.parse(data); 
        });
        
 
		  return {

		  	///////////////////////////////////////
		  	clearAll:function(key, value) {
                sharedDataObj = {}; 
		        //sharedDataObj["logState"] = localStorage.getItem("logState");
		        phpJS.getPHPStatus(function(state, data){
		        	sharedDataObj["phpStatus"] = JSON.parse(data); 
		        });                    
			},
			///////////////////////////////////////	
					  	
		  	///////////////////////////////////////
		  	add:function(key, value) {
                    sharedDataObj[key] = value;
			},
			///////////////////////////////////////	
			
		  	///////////////////////////////////////
		  	fetch:function(key) {
                  if ( sharedDataObj[key] == undefined ||  sharedDataObj[key] == null){
                  	return false;
                  }
                  else{
                  	return sharedDataObj[key];
                  }
			},
			///////////////////////////////////////				
			
			
		  	///////////////////////////////////////
		  	remove:function(key) {

			},
			///////////////////////////////////////	
			
		  	///////////////////////////////////////
		  	request:function(component, callback) {
		  		
		  		var counter = 0;
		  		function check(){
		  			setTimeout(function(){
			  			if (sharedDataObj[component] == undefined){
			  				counter++;
			  				if (counter < 100){
			  					check();
			  				}
			  				else{
			  					timeout();
			  				}
			  			}
			  			else{
			  				callback(true, sharedDataObj[component]);
			  			}
		  			}, 50);
		  		}
		  		
		  		function timeout(){
		  			callback(false, "Data has timedout.");
		  		};
				check();
			},
			///////////////////////////////////////	

		  	///////////////////////////////////////
		  	getAll:function() {
				return sharedDataObj;
			}
			///////////////////////////////////////							

    		
    
    
  		};
 
});


