// ESTABLISH REQUIREJS PATHING
require.config({
	
	
	// URL
	baseUrl: '',
	
	// LOCATION OF SCRIPTS
	paths: {
		
		// FIREBASE //		
		"firebase": "https://cdn.firebase.com/libs/angularfire/0.9.1/angularfire.min",						//https://www.firebase.com/quickstart/angularjs.html
		"firebaseAuthenticate": "https://cdn.firebase.com/js/simple-login/1.6.3/firebase-simple-login",		//https://www.firebase.com/docs/security/simple-login-email-password.html
		"firebaseJS": "https://cdn.firebase.com/js/client/2.1.0/firebase",		
		"firebaseSettings": "require/firebaseSettings",	
		
		// CORE FILES
		"angular": "https://ajax.googleapis.com/ajax/libs/angularjs/1.3.10/angular.min",				
		"jquery": "https://code.jquery.com/jquery-2.1.1.min",
		"slickCore": "http://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min",					//http://kenwheeler.github.io/slick/
		"slick": "vendor/angular/module/angular-slick",										//https://github.com/vasyabigi/angular-slick/tree/master/dist
		"backstretch": "vendor/backstretch/backstretch",
		"foundation": "vendor/foundation5/js/foundation.min",
		"transitJS": "vendor/angular/module/transit"	,										//http://ricostacruz.com/jquery.transit/		
		"responsiveTables": "vendor/foundation5/js/responsive-tables",	
		"mm.foundation": "vendor/foundation5/js/foundation5",									//http://madmimi.github.io/angular-foundation/	
		"angular-loading-bar": "vendor/angular/module/loading-bar",								//http://chieffancypants.github.io/angular-loading-bar/	
		"SmoothScroll": "vendor/angular/module/ng-smoothscroll.min",							//https://github.com/gsklee/ngStorage
		//"angular-carousel": "vendor/angular/module/angular-carousel.min",						//https://github.com/revolunet/angular-carousel
		"ngStorage": "vendor/angular/module/ngStorage.min",	
		"ngAnimate": "https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.10/angular-animate.min",							//http://code.angularjs.org/1.1.4/docs/api/ng.directive:ngAnimate
		"ngSanitize": "vendor/angular/module/angular-sanitize",									//https://github.com/angular/bower-angular-sanitize	
		"ui.router": "vendor/angular/module/angular-ui-router",									//https://github.com/angular-ui/ui-router			
		"formly": "vendor/angular/module/formly",												//https://github.com/nimbly/angular-formly		
		"angularLoad": "vendor/angular/module/angular-load.min",								//https://github.com/urish/angular-load	
		"app": "require/app",	
					
		// ESSENTIAL BASE MODULES //
		"readMore": "vendor/angular/module/angular-readmore",											//https://github.com/pattysnippets/angular-readmore
		"underscore": "vendor/underscore/underscore",			
		"angular-underscore": "vendor/underscore/angular-underscore.min",						//http://underscorejs.org/	
		"string": "vendor/angular/module/ng-string.min",										//https://github.com/goodeggs/ng-string"
		"truncate": "vendor/angular/module/truncate",											//https://github.com/sparkalow/angular-truncate
		"angular-centered": "vendor/angular/module/angular-centered",							//http://ngmodules.org/modules/angular-centered
		"angular-gap": "vendor/angular/module/angular-gap",								
		"adaptive.detection": "vendor/angular/module/angular-adaptive-detection.min",			//https://github.com/angular-adaptive/adaptive-detection
		"ngTouch": "https://ajax.googleapis.com/ajax/libs/angularjs/1.2.9/angular-touch.min",	//https://github.com/angular/bower-angular-touch
		"sticky": "vendor/angular/module/sticky",												//https://github.com/d-oliveros/angular-sticky						
		"textarea-fit": "vendor/angular/module/textareaFit",									//https://github.com/nikolassv/angular-textarea-fit
		"lrFileReader": "vendor/angular/module/fileReader",
		"ng-Linq": "vendor/angular/module/ngLinq",												//https://github.com/ViniciusMachado/ngLinq
		"ngBrowserInfo": "vendor/angular/module/ngBrowserInfo.min",								//https://github.com/transferwise/ng-browser-info
		"psResponsive": "vendor/angular/module/ps-responsive",									//https://github.com/lavinjj/angular-responsive
		"toaster": "vendor/angular/module/toaster",												//https://github.com/jirikavi/AngularJS-Toaster	
		"angularSpinkit": "vendor/angular/module/angular-spinkit",								//https://github.com/urigo/angular-spinkit
		
		// + MOMENTJS
		"momentCore":"vendor/moment/moment.min",
		"angularMoment":"vendor/angular/module/angular-moment",
	
		// LAYOUT //
		"overlayCtrl": "layout/overlay/overlay",
		"offcanvasCtrl": "layout/offcanvas/offcanvas",
		"sliderCtrl": "layout/slider/slider",
		"headerCtrl": "layout/header/header",
		"footerCtrl": "layout/footer/footer",			
		
		// MODALS //
		"uiModalCtrl": "modals/ui/ui-modalControls",									
		"accountModalCtrl": "modals/account/account-modalControls",
		
		// PAGE CONTROLLERS //
		"masterCtrl": "components/master/master",	
		"homeCtrl": "components/home/home",
		"adminCtrl": "components/admin/adminCtrl",
		"firstLoginCtrl": "components/firstLogin/firstLogin",
		
		"page2Ctrl": "components/page2/page2Ctrl",
		"page3Ctrl": "components/page3/page3Ctrl",
		"page4Ctrl": "components/page4/page4Ctrl",
		"page5Ctrl": "components/page5/page5Ctrl",
		"page6Ctrl": "components/page6/page6Ctrl",	

		// FACTORIES //
		"custom": "factory/custom",
		"sharedData": "factory/sharedData",
		"konami": "factory/konami",


		// REMOVE IF NOT NEEDED	
		/*
		"papaParse": "vendor/papaparse/papaparse.min",											//http://papaparse.com/			
		"contenteditable": "vendor/angular/module/angular-contenteditable",						//https://github.com/akatov/angular-contenteditable	
		"ngPatternRestrict": "vendor/angular/module/ng-pattern-restrict",						//https://github.com/AlphaGit/ng-pattern-restrict
		"ui.utils": "vendor/angular/module/ui-utils.min",										//http://angular-ui.github.io/
		"FBAngular": "vendor/angular/module/angular-fullscreen",								//https://github.com/fabiobiondi/angular-fullscreen		
		"dcbImgFallback": "vendor/angular/module/angular.dcb-img-fallback.min",					//http://ngmodules.org/modules/angular-img-fallback							
		
		"gd.ui.jsonexplorer": "vendor/jsonexplorer/js/gd-ui-jsonexplorer",						//https://github.com/Goldark/ng-json-explorer
		"angularFileUpload": "vendor/angular/module/angular-file-upload",						//https://github.com/danialfarid/angular-file-upload		
		"checklist-model": "vendor/angular/module/checklist-model",								//https://github.com/vitalets/checklist-model		
		"ngClickSelect": "vendor/angular/module/ng-click-select",								//https://github.com/adjohnson916/ng-click-select
		"multi-select": "vendor/multiSelect/js/angular-multi-select",							//http://isteven.github.io/angular-multi-select/		
		"angular-table": "vendor/angular/module/smartTable.min",								//http://lorenzofox3.github.io/smart-table-website/	
		"ngIdle": "vendor/angular/module/angular-idle",											//http://ngmodules.org/modules/ng-idle
		"angulartexteditor": "vendor/angular/module/angularTextEditor",                         //https://github.com/vitconte/angularTextEditor
		"ui.tree": "vendor/uiTree/js/angular-ui-tree.min",										//https://github.com/JimLiu/angular-ui-tree
		"ng-mfb": "vendor/floatingButtons/mfb-directive",										//https://github.com/nobitagit/ng-material-floating-button/blob/master/src/index.html
		"adaptive.youtube": "vendor/angular/module/angular-adaptive-youtube.min",				//https://github.com/angular-adaptive/adaptive-youtube
		
		// + FROALA TEXT EDITOR
		"froalaJS":"vendor/froala/js/froala_editor.min",
		"froala":"vendor/froala/js/angular-froala",
		"froalaSanitize":"vendor/froala/js/angular-froala-sanitize",		
		

		// +  D3 GRAPHS 
		"d3Core":"vendor/d3/js/d3.min",
		"angularCharts":"vendor/d3/js/angular-charts",											//http://chinmaymk.github.io/angular-charts/
		"ngDonut":"vendor/d3/js/donut",															//https://github.com/Wildhoney/ngDonut
		"n3-line-chart":"vendor/d3/js/lineCharts.min",											//https://github.com/n3-charts/line-chart
		*/
		

	},

    //DEPENDENCIES
    shim: {        
        

    	//FIREBASE
        "firebase": {
			deps: ['angular', 'firebaseJS', 'firebaseAuthenticate'],
        	exports: "angular"        	
        },   
        "firebaseSettings":{
        	deps: ['angular', 'firebase', 'firebaseAuthenticate']
        },        
        
            
        //CORE
		"custom":{
        	deps: ['angular', 'transitJS', 'momentCore', 'backstretch']
        },  
		"sharedData":{
        	deps: ['angular', 'firebase', 'firebaseAuthenticate']
        },            
        "angular": {
            exports: "angular"
        },  
        "slickCore":{
        	deps: ['jquery']
        },
        "slick":{
        	deps: ['slickCore', 'angular'],
        	exports: "angular"
        },
        "backstretch":{
        	deps: ['jquery']
        },
        "transitJS":{
        	deps: ['jquery']
        },      
        "responsiveTables": {
        	deps: ['jquery']
        },
        "mm.foundation": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        "angular-loading-bar": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },          
     	"SmoothScroll":{
			deps: ['angular'],
        	exports: "angular"  	
    	},
    	/*
        "angular-carousel": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },
        */    
        "ngStorage": {
        	deps: ['angular'],
        	exports: "angular"	
        },  
      
        "ngAnimate": {
        	deps: ['angular'],
        	exports: "angular"
        },
                       
        "ngSanitize": {
        	deps: ['angular'],
            exports: "angular"
        },          	
        "ui.router": {
        	deps: ['angular'],
            exports: "angular"
        },
        "formly":{
			deps: ['angular'],
        	exports: "angular"     	
        },  
        "angularLoad":{
        	deps: ['angular'],
        	exports: "angular"	
        },                 
        
                
        // ESSENTIAL
        "readMore":{
			deps: ['jquery', 'angular'],
        	exports: "angular"         	
        },         
        "angular-underscore":{
			deps: ['angular', 'underscore'],
        	exports: "angular"         	
        },         
        "string":{
        	deps: ['angular'],
        	exports: "angular"	
        },     
        "truncate":{
        	deps: ['angular'],
        	exports: "angular"
        },  
        "angular-centered": {
        	deps: ['angular'],
        	exports: "angular"
        },    
        "angular-gap":{
        	deps: ['angular'],
        	exports: "angular"
        },
         "adaptive.detection": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        "ngTouch": {
        	deps: ['angular'],
        	exports: "angular"
        },          
        "sticky": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "textarea-fit":{
			deps: ['angular', 'jquery'],
        	exports: "angular"         	
        },        
        "lrFileReader":{
			deps: ['angular'],
        	exports: "angular"       	
        }, 
        "ng-Linq":{
			deps: ['angular'],
        	exports: "angular"       	
        },          
        "ngBrowserInfo":{
			deps: ['angular'],
        	exports: "angular"       	
        }, 
        "psResponsive": {
        	deps: ['angular'],
        	exports: "angular"
        }, 
        
        "toaster": {
        	deps: ['angular'],
        	exports: "angular"	
        },     
        "angularSpinkit":{
			deps: ['angular'],
        	exports: "angular"        	
        },                  
      
        // MODALS
        "uiModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },
        "accountModalCtrl":{
			deps: ['angular'],
        	exports: "angular"   	
        },           
           
             
        // REMOVE IF NOT NEEDED   
		/*        
        "papaParse": {
        	exports: "papaParse"	
        },        
                           
        "contenteditable": {
        	deps: ['angular'],
        	exports: "angular"
        },  
        "ngPatternRestrict":{
			deps: ['angular'],
        	exports: "angular"    	
        },   
        "ui.utils": {
        	deps: ['angular'],
            exports: "angular"
        },          
 
        "FBAngular": {
        	deps: ['angular', 'ngTouch'],
        	exports: "angular"	
        },  
       
        "dcbImgFallback":{
        	deps: ['angular'],
        	exports: "angular"
        },
        

        "gd.ui.jsonexplorer":{
        	deps: ['angular'],
        	exports: "angular"
        },  
        "angularFileUpload":{
        	deps: ['angular'],
        	exports: "angular"
        },         
        "checklist-model":{
			deps: ['angular'],
        	exports: "angular"     	
        },
        "ngClickSelect":{
			deps: ['angular'],
        	exports: "angular"   	
        },  
        "multi-select":{
			deps: ['angular'],
        	exports: "angular"   	
        },  
        "angular-table":{
			deps: ['angular'],
        	exports: "angular"    	
        },    
        "ngIdle":{
			deps: ['angular'],
        	exports: "angular"    	
        }, 
        "ui.tree":{
			deps: ['angular'],
        	exports: "angular"   	
        },      
        "ng-mfb":{
			deps: ['angular'],
        	exports: "angular"  	
        },  
    	"adaptive.youtube":{
			deps: ['angular'],
        	exports: "angular"  		
    	},
     
        
        
		// FROALA
        "froalaJS":{
			deps: ['jquery']      	
        },
        "froalaSanitize":{
			deps: ['angular', 'froalaJS'],
        	exports: "angular"         	
        },
        "froala":{
			deps: ['jquery', 'froalaJS', 'angular', 'froalaSanitize'],
        	exports: "angular"         	
        },  
        
          
        
        // D3
        "angularCharts":{
			deps: ['angular', 'd3Core'],
        	exports: "angular"    	
        },
        "ngDonut":{
			deps: ['angular', 'd3Core'],
        	exports: "angular" 
        },
        "n3-line-chart":{
			deps: ['angular', 'd3Core'],
        	exports: "angular" 
        },
    	*/

    	
  
    }	
	
});
 	 


// INITALIZE ANGULAR IN CONTROLLERS
require(	[	// DEPENDENCIES
				'app', 'firebase',  
 				
 				// OVERLAY	
				'overlayCtrl', 'offcanvasCtrl', 'sliderCtrl', 
				
				// HEADER/FOOTER
				'headerCtrl', 'footerCtrl',
				
				// SETUP				
				'masterCtrl','adminCtrl', 'firstLoginCtrl',
				
				// PAGES
				'homeCtrl', 
				'page2Ctrl',
				'page3Ctrl',
				'page4Ctrl',
				'page5Ctrl',
				'page6Ctrl'
				 
			], 
				
	function (	app, firebase, 
				
				// OVERLAY
				overlayCtrl, offcanvasCtrl, sliderCtrl, 
				
				// HEADER/FOOTER
				headerCtrl, footerCtrl, 
				
				// SETUP
				masterCtrl, adminCtrl, firstLoginCtrl,
				
				// PAGES	
				homeCtrl, 
				page2Ctrl,
				page3Ctrl,
				page4Ctrl,
				page5Ctrl,
				page6Ctrl
				
			) {
	
				// OVERLAY CONTROLLERS
				overlayCtrl.apply(app);
				offcanvasCtrl.apply(app);
				sliderCtrl.apply(app);
				
				// HEADER/FOOTER
				headerCtrl.apply(app);
				footerCtrl.apply(app);
						
				// SETUP
				masterCtrl.apply(app);
				firstLoginCtrl.apply(app);		
				adminCtrl.apply(app);
				
				// PAGES
				homeCtrl.apply(app);						
				page2Ctrl.apply(app);
				page3Ctrl.apply(app);
				page4Ctrl.apply(app);
				page5Ctrl.apply(app);
				page6Ctrl.apply(app);
				
				app.init();
	
});


