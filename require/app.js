define("app", 	[	
					// FACTORIES
					"custom", "firebaseSettings", 
					
					// FIREBASE
					"firebase",
					
					// CORE
					"angular",
					"uiModalCtrl", 
					"accountModalCtrl",
					"slick",
					//"angular-carousel", 
					"responsiveTables",
					"mm.foundation", 
					"ngStorage",
					"ngAnimate", 
					"ngSanitize",
					"SmoothScroll",
					"angular-loading-bar",
					"ui.router", 
					"angularLoad",
					"formly",
					
					// ESSENTIAL
					"readMore",
					"angular-underscore",					
					"string",
					"truncate",
					"angular-centered", 
					"angular-gap", 
					"adaptive.detection", 
					"sticky", 
					"textarea-fit",
					"lrFileReader", 
					"ng-Linq", 
					"ngBrowserInfo",
					"psResponsive",
					"toaster", 	
					"angularSpinkit",		
							
					// NOT REQUIRED - REMOVE IF NOT NEEDED
					/*
					"contenteditable",
					"ngPatternRestrict",  
					"ui.utils",
					"FBAngular", 
					"dcbImgFallback",
					
					"gd.ui.jsonexplorer",  	
					"angularFileUpload", 
					"checklist-model", 
					"ngClickSelect", 
					"multi-select",
					"angular-table", 
					"ngIdle",
					"ui.tree",
					"ng-mfb",
					"adaptive.youtube",

					// FROALA
					"froala",
					
					// D3 CHARTS
					"angularCharts", "ngDonut", "n3-line-chart"
					*/
					
				], function(custom, firebaseSettings) { 
	
	
	
	////////////////
	// INIT 
	var app = angular.module('app', [	
	
					
					// FIREBASE
					"firebase",
					
					// CORE
					"uiModalCtrl", 
					"accountModalCtrl",
					"slick",
					//"angular-carousel",
					"ngStorage",
					"ngAnimate", 
					"ngSanitize",
					"SmoothScroll",
					"mm.foundation", 
					"angular-loading-bar", 	
					"ui.router", 
					"angularLoad", 
					"formly",
										
					// ESSENTIAL
					"readMore",
					"angular-underscore", 
					"string", 
					"truncate", 
					"angular-centered",
					"angular-gap", 	
					"adaptive.detection", 						
					"sticky", 
					"textarea-fit",
					"lrFileReader", 
					"ng-Linq",
					"ngBrowserInfo",
					"psResponsive",  
					"toaster",	
					"angularSpinkit", 		
					
					// NOT REQUIRED -
					/*
					"contenteditable",
					"ngPatternRestrict", 	
					"ui.utils",
					"FBAngular",					
					"dcbImgFallback", 					 
					"ngIdle",						 		
					
					"gd.ui.jsonexplorer", 
					"angularFileUpload", 
					"checklist-model", 
					"ngClickSelect", 
					"multi-select",
					"angular-table",
					"ui.tree", 
					"ng-mfb",	
					"adaptive.youtube", 								
					
					// FROALA
					"froala",
					
					// D3 CHARTS
					"angularCharts", "ngDonut", "n3-line-chart"
					*/
	]);
										
	app.init = function () {
		
		firebaseSettings.setDefaultUserData(function(){
			angular.bootstrap(document, ['app']);	
		});
		
	           
	};
	////////////////
	
	// let's make a nav called `myOffCanvas`
	app.factory('myOffCanvas', function (cnOffCanvas) {
	  return cnOffCanvas({
	    controller: 'headerCtrl',
	    templateUrl: 'components/offcanvas/offcanvas.php'
	  });
	}).
	
	value('froalaConfig', {
			inlineMode: false,
			events : {
				align : function(e, editor, alignment){
					console.log(alignment + ' aligned');
				}
			}
	}).
	
	// typically you'll inject the offCanvas service into its own
	// controller so that the nav can toggle itself
	controller('MyOffCanvasCtrl', function (myOffCanvas) {
	  this.toggle = myOffCanvas.toggle;
	}).
		
		
	directive('styleParent', function(){ 
	   return {
	     restrict: 'A',
	     link: function(scope, elem, attr) {
	         elem.on('load', function() {
	            var w = $(this).width(),
	                h = $(this).height();
				
					//$('#mainSlider').css('min-height', h + 'px');
					//$('#headerFiller').css('height', h + 'px'  ) ;
	            //check width and height and apply styling to parent here.
	         });
	     }
	   };
	}).	
		
	////////////////  CONFIG
	//
	config(function($stateProvider, $urlRouterProvider) {
			
			// For any unmatched url, redirect to /state1
			$urlRouterProvider.otherwise("/home");
		  
			// Now set up the states
			$stateProvider
			    .state('home', {
			      url: "/home",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "slider": { templateUrl: "layout/slider/slider.php",
			        			controller: "sliderController"
			        },				        
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/home/home.php",
			        			controller: "homeController"
			         },			         
			      },	
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },				  			      		      
			      
			    })			    
			    .state('admin', {
			      url: "/admin",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/admin/admin.php",
			        			controller: "adminController"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('page2', {
			      url: "/page2",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page2/page2.php",
			        			controller: "page2Controller"
			         },
			      },
			      onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },
			    })
			    
			    .state('page3', {
			      url: "/page3",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },		
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        				        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page3/page3.php",
			        			controller: "page3Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('page4', {
			      url: "/page4",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page4/page4.php",
			        			controller: "page4Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('page5', {
			      url: "/page5",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },	
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        					        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page5/page5.php",
			        			controller: "page5Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })

			    .state('page6', {
			      url: "/page6",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        						        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/page6/page6.php",
			        			controller: "page6Controller"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    })
			    
			    .state('firstLogin', {
			      url: "/firstLogin/:email/{tempPassword}",
			      views: {
			        "overlay": { templateUrl: "layout/overlay/overlay.html",
			        			controller: "overlayController"
			        },				      				      	
			        "offcanvas": { templateUrl: "layout/offcanvas/offcanvas.html",
			        			controller: "offcanvasController"
			        },	
			        "ui-modals": { templateUrl: "modals/ui/ui-modals.html"
			        			
			        },
			        "account-modals": { templateUrl: "modals/account/account-modals.html"
			        			
			        },				        						        			        			        			        			      	
			        "header": { templateUrl: "layout/header/header.php",
			        			controller: "headerController"
			        },
			        "footer": { templateUrl: "layout/footer/footer.php",
			        			controller: "footerController"
			        },			        			       
			        "body": { 	templateUrl: "components/firstLogin/firstLogin.php",
			        			controller: "firstLoginController"
			         },
			      },
				  onEnter: function(){
				   custom.transitionStart();
				  },
				  onExit: function(){
				    custom.transitionEnd();
				  },			      	
			    });			     
			    			    
			    

	    			       
		});
		//
		////////////////   
	
	return app;
	
});

