define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	
	angular.module('accountModalCtrl', []).	
	factory('accountModalCtrl', function(){
		
		
		return {

			//////////////////////
			/* TEST CONTROLLER */
			ping: function(amount){								
				var test = "account pong: " + amount;
				return 	test;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////
			

			//////////////////////
			/* CREATE MODAL CONTROLLER */
			createModalCtrl: function(){								
				var cntrl = function ($scope, $modalInstance,  $localStorage, $sessionStorage, $firebase, data) {
					
					// MODAL DATA
					$scope.modalData = {
						//name: data.name
					};
					

					
				  	// SETUP
					$scope.formOptions = {
						uniqueFormId: 'simpleform',
						submitCopy: 'Create Account'
					};	
					$scope.submittedData = new Object; 
					$scope.formData = {};	
					
					
					// UPLOAD ONE OR MULTIPLE IMAGES
					$scope.resizeData = {
						sizes 	: "35, 100, 200",
						quality : "70",
						type    : "image/jpeg"
					};
					
					$scope.quality = 70;  
					//////////////////////// 					
					

					// FORM FIELDS
					$scope.formFields = [					
						{	
							key: 'firstName',
							type: 'text',
							label: 'First Name',
							placeholder: 'Jane',
							required: true,
						}, 
						{
							key: 'lastName',
							type: 'text',
							label: 'Last Name',
							placeholder: 'Doe',
							required: true,
						},
						{
							key: 'email',
							type: 'email',
							placeholder: 'janedoe@gmail.com',
							key: 'email',
							required: true,
						}
					];
					$scope.isChecking = false; 
					$scope.isUpload   = false; 
					
					// ON SUBMIT
					$scope.submit = function () {
						

						$scope.isChecking = true; 

						var firebaseData = new Firebase( firebaseSettings.firebaseRoot() );	
						var auth = new FirebaseSimpleLogin(firebaseData, function(error, user) {	});		
						var newPassword = Math.random().toString(36).substring(7);							

				
						auth.createUser($scope.formData.email, newPassword, function(error, user) {						  
						  
						  
						  if (!error) {
						    
						    $scope.userId = user.id; 					  	
						  	var root = firebaseSettings.firebaseRoot();
						  	var userPosts	 = root  + "/users/" + user.id + "/info/";
								
								
								// SEND EMAIL
								sharedData.request('url', function(state, data){

							  		// SEND USER AN EMAIL WITH TEMP PASSWORD
							  		var toAccount = data.full + "#/firstLogin/" + $scope.formData.email + "/" + newPassword;
									
									var emailData = {
										sendTo: $scope.formData.email,
										subject: "Welcome!",
										content: "<h3>Hello there!</h3><p><a href='" + toAccount + "'>Click here to activate your account.</a></p>",
										replyTo: "no-replay@codeandlogic.com"	
									};
								
									phpJS.sendEmail(emailData, function(state, data){
											//console.log(state, data);
									});	
						  											
									
								});

								
						  		// SET DEFAULT USER INFORMATION
				            	var firebase_byUser = new Firebase(userPosts);
				            	var entrydata = 	{	
				            							id: user.id,
				            							email: $scope.formData.email,
				            							permission: "user",		 
				            							userName:  $scope.formData.firstName + " " +  $scope.formData.lastName,
				            							firstName: $scope.formData.firstName,
				            							lastName:  $scope.formData.lastName
				            							           											            					
				            						};
										            						
								
				            	firebase_byUser.setWithPriority( entrydata, user.id );	
				            	
				            	
					       		// SET DEFAULT IMAGES
								var userImages	 = root  + "/users/" + user.id + "/images/profile/";
								var firebase_byUser = new Firebase(userImages);
				            	var imageData = 	{	
				            							thumbnail: 'http://dummyimage.com/100x100/000/fff',
				            							small: 'http://dummyimage.com/100x100/000/fff',
				            							standard: 'http://dummyimage.com/100x100/000/fff'		            											            					
				            						};						
								
								firebase_byUser.setWithPriority( imageData, user.id );		// CHANGE OUT FIREBASE
								localStorage.setItem("images", JSON.stringify(imageData) );					// CHANGE OUT LOCALSTORAGE
								
								
								// APPLY
								$scope.isUpload = true; 
				            	$scope.isChecking = false;
				            	$scope.$apply();
				            	
						  							    
						  }				
						  else{
						  	alert(error);	
						  	$scope.isChecking = false; 
						  	$scope.$apply();
						  }	  
						});							
						
						$modalInstance.close($scope.modalData);						  	
					};
					
					$scope.uploadImage = function(data){
					
						var root = firebaseSettings.firebaseRoot();
						var userImages	 = root  + "/users/" + $scope.userId + "/images/profile/";
						var firebase_byUser = new Firebase(userImages);
		            	var entrydata = 	{	
		            							thumbnail: data[0].src,
		            							small: data[1].src,
		            							standard: data[2].src		            											            					
		            						};						
						
						firebase_byUser.setWithPriority( entrydata, $scope.userId );	
						$scope.close();						
					};
					  
					$scope.cancel = function () {
					  	;
					    $modalInstance.dismiss();
					};
					  
					$scope.close = function(){				  	
					  	;
					  	$modalInstance.close('');
					};
				};	
				return 	cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////	


			
			//////////////////////
			/* MODAL CONTROLLER */
			deleteModalCtrl: function(){								
				var cntrl = function ($scope, $timeout, $modalInstance,  $localStorage, $sessionStorage, $firebase, data) {
					
					
					// DEFAULT 							
					$scope.userForm = {
						password: '', confirm: ''
					};					
					$scope.isThinking = false;
					
					
					// SETUP FIREBASE
					var firebaseData = new Firebase( firebaseSettings.firebaseRoot() );	
					var auth = new FirebaseSimpleLogin(firebaseData, function(error, user) {});	
					

					
					// BEGIN DELETE
					$scope.beginDelete = function () {
							
							if ($scope.userForm.password == $scope.userForm.confirm ){
								$scope.isThinking = true;
								firebaseSettings.checkUserData(function(returnState, userData){	
									auth.removeUser(userData.user.email, $scope.userForm.password, function(error, success) {
									  if (error) {
									    alert("Password does not match the one in the database.  Please try again.");	
									    $scope.isThinking = false;
									    $scope.$apply();						      
									  }
									  else{					
									  	
									  	// DELETE USER INFORMATION				
									  	var root = firebaseSettings.firebaseRoot(); 
										var user = root + "/users/" + userData.user.id;
										var onComplete = function(error) {
										  if (error){  	
										  	console.log("Error occured.");
										  }
										  else{
										  		
											  	$scope.close();
											  		  
										  }
										};
										
										var deleteUser	= new Firebase(user);
										deleteUser.remove(onComplete);									
									  	
									  }
									});								
								});
							}
							else{
								alert("Passwords do not match.  Please try again.");
							}						
										  	
					};
					 
					// CANCEL 
					$scope.cancel = function () {
					  	;
					    $modalInstance.dismiss();
					};
					
					// CLOSE  
					$scope.close = function(){				  	
					  	;
					  	$modalInstance.close('');
					};
				};	
				return 	cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////		
			
			////////////////////// 
			/* MODAL CONTROLLER */
			editProfileModalCntrl: function(){		
					
				var cntrl = function ($scope, $modalInstance,  $timeout, lrFileReader) {
						
			
						
				        $scope.handleFileSelect = function(evt) {
				        	
						var file = evt.files[0];	
										        	
					    lrFileReader(file)
					        .on('progress',function(event){
					            $scope.progress = event.loaded/event.total;
					        })
					        .on('load',function(event){
					        })
					        .readAsDataURL()
					        .then(function(result){
					        	custom.imageToDataUri(result, 200,  function(imgData, dimensions){
					        		$scope.imageData = imgData;
					        		$scope.dimensions =  dimensions; 
					        		$scope.newProfileImg = true;
					        		$scope.$apply();
					        	});		      
					        });		
    
				        };
						
						$scope.uploadImage = function(data){
							
							firebaseSettings.checkUserData(function(returnState, userData){	
								var root = firebaseSettings.firebaseRoot();
								var userImages	 = root  + "/users/" + userData.user.id + "/images/profile/";
								var firebase_byUser = new Firebase(userImages);

				            	var entrydata = 	{	
				            							thumbnail: data,
				            							small: data,
				            							standard: data		            											            					
				            						};						
								

								firebase_byUser.setWithPriority( entrydata, userData.user.id  );		// CHANGE OUT FIREBASE								
								$scope.close();
								
							});
						};
					
						$scope.close = function () {
						  	;
						    $modalInstance.close('');
						};

						$scope.cancel = function () {
						  	;
						    $modalInstance.dismiss('cancel');
						};
						  
					};	
				return 	cntrl;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////		
			
			
			
			////////////////////// 
			/* MODAL CONTROLLER */
			changePasswordCtrl: function(){								
				var cntrl = function ($scope, $modalInstance,  $timeout) {
						
						$scope.userData = {
							currentPassword: '',
							newPassword: '',
							confirmPassword: ''
						};
						var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
						var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {		});									
						$scope.isThinking = false;
						
						$scope.update = function(){
						
							
							// NO MATCHES
							if ($scope.userData.newPassword.length < 8){
								alert("Password must be at least 8 characters in length.");
							}
							else if ($scope.userData.newPassword != $scope.userData.confirmPassword){
								alert("Passwords do not match.  Please try again.");
							}else if($scope.userData.newPassword == $scope.userData.currentPassword){
								alert("New password cannot be the same as your old one.");
							}
						
							
							// MATCHES, CONTINUE TO UPDATE
							else{		
								$scope.isThinking = true;
												
								firebaseSettings.checkUserData(function(returnState, userData){	
									auth.changePassword(userData.user.email, $scope.userData.currentPassword, $scope.userData.newPassword, function(error, success) {
									  							  
									  if (!error) {									   
									  	;
									    $modalInstance.close('changed');
									  }
									  else{							  	
									  	$scope.isThinking = false;
									  	$scope.$apply();
									  	alert(error);
									  }
									});								
								});								
							}						
						};
						 
					
						$scope.close = function () {
						  	;
						    $modalInstance.close('');
						};

						$scope.cancel = function () {
						  	;
						    $modalInstance.dismiss('cancel');
						};
						  
					};	
				return 	cntrl;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////						
			

			////////////////////// 
			/* MODAL CONTROLLER */
			editProfileDataModalCntrl: function(){								
				var cntrl = function ($scope, $modalInstance,  $timeout) {
				      
						$scope.pageLoading = true; 
						sharedData.request('userInfo', function(state, data){   
							
							$scope.pageLoading = false; 	
							$scope.userData = data;	
							
							$scope.formData = {};
							$scope.formFields = [	
								{
									key: 'userName',
									type: 'text',
									label: 'User Name',
									placeholder: 'Allen the Awesome',
									default:  $scope.userData.userName
								}, 				
								{	
									key: 'firstName',
									type: 'text',
									label: 'First Name',
									placeholder: 'First Name',
									default: $scope.userData.firstName
								}, 
								{
									key: 'lastName',
									type: 'text',
									label: 'Last Name',
									placeholder: 'Last Name',
									default: $scope.userData.lastName
								},
								{
									key: 'email',
									type: 'email',
									placeholder: 'janedoe@gmail.com',
									key: 'email',
									default: $scope.userData.email,
									required: true
								}
							];						
							$scope.$apply();				
							
						});
						
						$scope.isChecking = false; 
						$scope.update = function(){
							
							$scope.isChecking = true;
							var infoEntry = new Firebase( firebaseSettings.firebaseRoot() + '/users/' + $scope.userData.id + "/info/");														
								infoEntry.update({email: $scope.formData.email});	
								infoEntry.update({firstName: $scope.formData.firstName});	
								infoEntry.update({lastName: $scope.formData.lastName});	
								infoEntry.update({userName: $scope.formData.userName});	
							
							$scope.close();
							
						};
			
						$scope.close = function () {
						  	;
						    $modalInstance.close('');
						};

						$scope.cancel = function () {
						  	;
						    $modalInstance.dismiss('cancel');
						};
					
						
					};	
					
				return 	cntrl;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////				
			
			
			//////////////////////
			/* MODAL CONTROLLER */
			loginModalCtrl: function(){								
				var cntrl = function ($scope, $modalInstance,  $localStorage, $sessionStorage, $firebase) {
					
					////////////////////// USE OBJECT OR SCOPE WONT WORK
					$scope.selected = {
						username: "",
						password: "",					
					};
					$scope.isLoading = false; 
					//////////////////////
					
					// FIREBASE
					var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );	
					//////////////////////
					
					// RESET EMAIL
					$scope.reset = function(){
						
						if ($scope.selected.username != ''){
							
							$scope.isLoading = true; 
							var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {});

							//PASSWORD RESET							
							auth.sendPasswordResetEmail($scope.selected.username, function(error, success) {
								$scope.isLoading = false;
								$scope.$apply(); 
								if (!error) {
								    alert('Password reset email sent successfully');
								    
								}
								else{
								  	alert("Email address does not match our database. Please try again.");
								}
							});						
						
						
						}
						else{
							alert("Please enter an email.");
						}
						
						
					};
					//////////////////////					
					

					// CONTROLS	
						
					$scope.ok = function () {	
					  	$scope.isLoading = true; 
					  	
					  		
							var checkOnce = false; 
							var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {
								if (!checkOnce){
								 	if (error == null){
								 		if (user !== null){
								 			checkOnce = true; 					 			
											firebaseSettings.setLoginData(user, false);		// SET LOGIN DATA						 			 										 										 		
								 			$modalInstance.close("");			  					
								 		}
								 	}
								 	else{
								 		checkOnce = true; 
										$scope.isLoading = false;
										$scope.$apply(); 
									}
								}
								
							});		
							auth.login('password', {  // PASSWORD IS THE METHOD ()
							  email:		$scope.selected.username,
							  password: 	$scope.selected.password
							});		
			  				
					  	
					};
					
					$scope.cancel = function () {
					  	;
					    $modalInstance.dismiss();
					};
					  
					$scope.close = function(){				  	
					  	;
					  	$modalInstance.dismiss();
					};				
					//////////////////////
					
				};	
				
				return 	cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////		


			//////////////////////
			/* MODAL CONTROLLER */
			resetEmailCtrl: function(){								
				var cntrl = function ($scope, $modalInstance,  $localStorage, $sessionStorage, $firebase) {
					
					////////////////////// USE OBJECT OR SCOPE WONT WORK
					$scope.selected = {
						username: "",				
					};
					$scope.isLoading = false; 
					//////////////////////
					
					// FIREBASE
					var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );	
					//////////////////////
					
					// RESET EMAIL
					$scope.reset = function(){
						
						if ($scope.selected.username != ''){
							
							$scope.isLoading = true; 
							var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {});

							//PASSWORD RESET							
							auth.sendPasswordResetEmail($scope.selected.username, function(error, success) {
								$scope.isLoading = false;
								
								if (!error) {
								    $modalInstance.close("");									    
								}
								else{
								  	alert("Email address does not match our database. Please try again.");							  	
								}
							});						
						
						
						}
						else{
							alert("Please enter an email.");
						}
						
						
					};
					//////////////////////					

					
					$scope.cancel = function () {
					  	;
					    $modalInstance.dismiss();
					};
					  
					$scope.close = function(){				  	
					  	;
					  	$modalInstance.dismiss();
					};				
					//////////////////////
					
				};	
				
				return 	cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////	
					
			
		};
		

	
	});

});