define(['custom'], function(custom) {


	angular.module('uiModalCtrl', []).	
	factory('uiModalCtrl', function(){
		
		return {

			//////////////////////
			/* TEST CONTROLLER */
			ping: function(amount){								
				var test = "ponged: " + amount;
				return 	test;			
			},
			/* END MODAL CONTROLLER */
			//////////////////////
			
			//////////////////////
			/* MODAL CONTROLLER */
			confirmModalCtrl: function($scope, $modalInstance){		
				$scope.yes = function () {				  
				    $modalInstance.close(true);				  	
				};
				  
				$scope.no = function () {
				    $modalInstance.close(false);
				};
				  
				$scope.cancel = function(){				  	
				  	$modalInstance.dismiss();
				};
				
				$scope.dismiss = function(){				  	
				  	;
				  	$modalInstance.dismiss();
				};					
			
			},
			/* END MODAL CONTROLLER */	
			//////////////////////				
			
			
			//////////////////////
			/* TEST CONTROLLER */
			choiceModalCtrl: function(items, $scope, $modalInstance){								
			  $scope.items = items;
			  $scope.selected = {
			    item: $scope.items[0]
			  };
			 
			  $scope.ok = function () {	
			    $modalInstance.close($scope.selected.item);
			  };
			
			  $scope.cancel = function () {
			    $modalInstance.dismiss('cancel');
			  };			
			},
			/* END MODAL CONTROLLER */
			//////////////////////			
				
				
			//////////////////////
			/* MODAL CONTROLLER */
			splashModalCtrl: function(){								
				var cntrl = function ($scope, $modalInstance) {
					
					var randomQuotes = [
						"Loading all the important stuff for you!",
						"Remember how important you are today.",
						"Keep it sexy, internet.",
					];
					
					$scope.randomQuote = randomQuotes[Math.floor((Math.random() * randomQuotes.length) + 0)];					
				};	
				return cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////		
				
			//////////////////////
			/* MODAL CONTROLLER */
			thinkingModalCtrl: function(){								
				var cntrl = function ($scope, $modalInstance) {
	
					
				};	
				return cntrl;		
			},
			/* END MODAL CONTROLLER */	
			//////////////////////			
				


			
			
			
			/* MODAL CONTROLLER */
			browserInfoCtrl: function(){	
				var cntrl = function ($scope, $modalInstance, $detection,  browserInfo) {

					///////////////////////	 DETECTION   
				  	if ($detection.isAndroid()){
				  		$scope.detected = "media/detection/androidIcon.jpg";
				  		$scope.detectedType = "Android OS";
				  	}
				  	else if($detection.isiOS()){
				  		$scope.detected = "media/detection/iosIcon.jpg";
				  		$scope.detectedType = "IOS";
				  	}
				  	else if($detection.isWindowsPhone()){
				  		$scope.detected = "media/detection/windowsIcon.jpg";
				  		$scope.detectedType = "Windows 8 (Mobile)";
				  	}
				  	else{
				  		$scope.detected = "media/detection/desktopIcon.jpg";
				  		$scope.detectedType = "Desktop";
				  	}
					///////////////////////	   						
					
					$scope.details = browserInfo.giveMeAllYouGot(); 
					  
					$scope.dismiss = function(){				  	
					  	;
					  	$modalInstance.dismiss();
					};
				};	
		
				return cntrl;		
			},				
			/* END MODAL CONTROLLER */										
						
			
		};
		

	
	});

});