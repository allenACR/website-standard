define(['custom', 'sharedData'], function(custom, sharedData) {
	var fileName  = 'footer';
  	
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
				app.controller(fileName + 'Controller', function($rootScope, $scope, $timeout) {	   

//-------------------------------------				    
				    // ---------------- VARIABLES 
				    $scope.page = {
				    	loadComponents: custom.fillArray(2),
				    	isLoaded: false,
				    };
				   	//-----------------
				      				    				      
				      
					// ---------------- INIT
					$scope.init = function(){
						loadComponents(function(){
							start();
						});					
					};
					//-----------------
					
					// ---------------- LOAD COMPONENTS
					function loadComponents(callback){
						checkMaster(function(state){
							if (state){
								$scope.page.loadComponents[0] = true;
								checkLoad(callback);
							}
						});
						doSomething(function(state){
							if (state){
								$scope.page.loadComponents[1] = true;
								checkLoad(callback);
							}
						});							
					};
					//-------------------
					
					
					// ------------------ SAMPLE COMPONENTS
					function checkMaster(callback){
						// wait for master.js to finish loading 		
						sharedData.request("masterReady", function(state, data){
							if(state){
								if(data.ready == true){
									callback(true);
								}
								else{
									alert(data.ready);
								}
							}
							else{
								alert(data);
							}	
						});	
					}
					function doSomething(callback){
						// LOAD A COMPONENT						
						callback(true);
					}
					//-----------------
					
					
					//----------------- CHECK LOAD
					function checkLoad(callback){
						var check = true,
							array = $scope.page.loadComponents; 					
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						// all loads completed
						if (check){		
							$scope.page.isLoaded = true;					
							$scope.masterData = sharedData.getAll();	
							$scope.$apply();
							callback();												
						}						
					};						
					//-----------------
//-------------------------------------













//-------------------------------------					
					//----------------- START 
					function start(){

						// FOOTER IS READY - INSERT CODE HERE
						
					}
					//-----------------

					//-----------------
					$scope.browserInfo = function(){
							callController({who: "master", action: "browserInfo"}, function(data){
								console.log(data);
							});
					};
					//-----------------					
//-------------------------------------					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
//-------------------------------------					
					// ----------------
					// COMMUNICATE BETWEEN CONTROLLERS	  
					$scope.$on(fileName + '_recieve', function(e, data) { runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
	
					var runRequest = (function () {
					  var execute = function (data) {
							switch(data.execute.name) {
								
								 // required for callbacks to master controller
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;
							     // add to this list if you need to call this specific controller
							    case "foo":
							        $scope._watched.callback = data.returnData.data;
							    break;
							    case "bar":
							        $scope._watched.callback = data.returnData.data;
							    break;   
							    
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };		
					})();
					
					// shorthand for calling 
					var callController = function(e, returnPacket){
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 
							
							var packet = {
									info:{
										to: e.who,
										from: fileName
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
							
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});						
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
						  var execute = function(packet, callback){
						  		packet["order"] = executeOrder.length;
						  		
						  		executeOrder.push(packet);
								$scope._watched = {execute: null, callback: null};
								var unbindWatch = $scope.$watch('_watched.execute', function() {	 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};
							      	
							    });
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// ----------------
//-------------------------------------					
				    
				});
				

				
				
				
							
								
	    },
	    ///////////////////////////////////////
  };
});
