<div class="" style='' ng-init="init()">
	<div id='footer-div'>

		<!--  FOR MOBILE -->
		<div ng-if="masterData.browserSize == 'small'">		
			<div ng-include  src="'layout/<?php $path = basename(__DIR__);echo $path;?>/mobile.html'"></div>
		</div>
			
		<!--  FOR DESKTOPS -->
		<div ng-if="masterData.browserSize == 'medium' || masterData.browserSize == 'large'">
			<div ng-include  src="'layout/<?php $path = basename(__DIR__);echo $path;?>/standard.html'"></div>	
		</div>	
		
	</div>
</div>