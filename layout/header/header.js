define(['custom', 'sharedData'], function(custom, sharedData) {
	var fileName  = 'header';
  	
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
			
				app.controller(fileName + 'Controller', function($scope, $timeout, $rootScope, browserInfo, psResponsive, $localStorage, $sessionStorage, $detection, $modal, uiModalCtrl, accountModalCtrl) {	   
				   $scope.fileName = fileName;
				  
//-------------------------------------
				    // ---------------- VARIABLES 
				    function resetVariables(){
				      $scope.page = {
				    	  loadComponents: custom.fillArray(2),
				    	  isLoaded: false,
				      };
				      
			
			            // 1st ngRepeat demo
			            $scope.slides = [	{id: 1, name: 'home', icon: ''},
			            					{id: 2, name: 'admin', icon: ''},
			            					{id: 3, name: 'page2', icon: ''}
			            				];				    
				    				      
				    }
				   	//-----------------
				      				    				      
				      
					// ---------------- INIT
					$scope.init = function(){
						resetVariables();
						loadComponents();										
					};
					//-----------------
					
					//----------------- REFRESH
					function onRefresh(callback){
						$scope.masterData = sharedData.getAll();
						$scope.$apply();		
					};
					//-----------------					
					
					// ---------------- LOAD COMPONENTS
					function loadComponents(){
						checkMaster(function(state){
							if (state){
								$scope.page.loadComponents[0] = true;
								checkLoad();
							}
						});
						bar(function(state){
							if (state){
								$scope.page.loadComponents[1] = true;
								checkLoad();
							}
						});	
												
					};
					//-------------------
					
					
					// ------------------ SAMPLE COMPONENTS
					function checkMaster(callback){
						// wait for master.js to finish loading 		
						sharedData.request("masterReady", function(state, data){
							if(state){
								if(data.ready == true){
									callback(true);
								}
								else{
									alert(data.ready);
								}
							}
							else{
								alert(data);
							}	
						});	
					}
					function bar(callback){
						// LOAD A COMPONENT						
						callback(true);
					}
					//-----------------
					
					
					//----------------- CHECK LOAD
					function checkLoad(){
						var check = true,
							array = $scope.page.loadComponents; 					
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						// all loads completed
						if (check){		
							$scope.page.isLoaded = true;					
							$scope.masterData = sharedData.getAll();
							$scope.$apply();	
							start();													
						}						
					};						
					//-----------------
//-------------------------------------












//-------------------------------------					
					//----------------- START 
					function start(){
						//console.log( $scope.masterData.browserDetails.mobile )
					}
					//-----------------
							 			
		 			//-----------------
		 			$scope.logout = function(){
							callController({who: "master", action: "logout"}, function(data){});
		 			};
		 			//-----------------
	  
				 	//----------------- OPEN LOGIN MODAL			 	
					$scope.login = function () {
						callController({who: "master", action: "login"}, function(data){
							if(data.data == "updated"){ 
								$timeout(function(){
									callController({who: "master", action: "showThinking"});
								}, 300);
								$timeout(function(){
									callController({who: "master", action: "refreshData"});
								}, 600);
							}
							
						});
					};
					//-----------------							
//-------------------------------------














//-------------------------------------
					// ----------------  PING/PONG	  		
					$scope.pong = function(callback){
						callback({status: fileName, msg: "pong"});
					};
					// ---------------- 		
					// -----------------
					// COMMUNICATE BETWEEN CONTROLLERS	  
					$scope.$on(fileName + '_recieve', function(e, data) { runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
	
					var runRequest = (function () {
					  var callback = function(data, returnData){
						var packet = {
								info:{
									to: data.info.from,
									from: fileName
								},
								execute: {
									name: "calledBack",
									callback: false
								},
								returnData: {
									data: returnData
								}
							};					
						$scope.broadcast(packet);
					  };						
						
					  var execute = function (data) {
							switch(data.execute.name) {
								
								 // required for callbacks to master controller
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;
							     // add to this list if you need to call this specific controller
							    case "refresh":
							        onRefresh();
							    break;								     
							    case "ping":
							        $scope.pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							   
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };		
					})();
					
					// shorthand for calling 
					var callController = function(e, returnPacket){
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 
							
							var packet = {
									info:{
										to: e.who,
										from: fileName
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
							
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});						
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
						  var execute = function(packet, callback){
						  		packet["order"] = executeOrder.length;
						  		
						  		executeOrder.push(packet);
								$scope._watched = {execute: null, callback: null};
								var unbindWatch = $scope.$watch('_watched.execute', function() {	 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};
							      	
							    });
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// ----------------
//-------------------------------------				   

					
				});			
				
				



				
								
					
	    },
	    ///////////////////////////////////////
  };
});
