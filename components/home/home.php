<div id='<?php $path = basename(__DIR__);echo $path;?>_id' class="content-default-height " ng-init="init()">

		<!--  FOR MOBILE -->
		<div ng-if="masterData.browserSize == 'small'">		
			<div ng-include  src="'components/<?php $path = basename(__DIR__);echo $path;?>/mobile.html'"></div>
		</div>
			
		<!--  FOR DESKTOPS -->
		<div ng-if="masterData.browserSize == 'medium' || masterData.browserSize == 'large'">
			<div ng-include  src="'components/<?php $path = basename(__DIR__);echo $path;?>/standard.html'"></div>	
		</div>	
	
			
</div>	




