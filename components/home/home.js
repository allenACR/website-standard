define(['custom', 'sharedData'], function(custom, sharedData) {

	
	var fileName  = 'home';
  
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
				app.controller(fileName + 'Controller', function($rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar) {	   
				    
				    
				    
//-------------------------------------				    
				    // ---------------- VARIABLES 
				    function resetVariables(){
				    	$scope.page = {
				    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
				    		isLoaded: false,
				    	};
    					$scope.content = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit. Nulla facilisi. Nulla libero. Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum. Donec porttitor ligula eu dolor. Maecenas vitae nulla consequat libero cursus venenatis. Nam magna enim, accumsan eu, blandit sed, blandit a, eros." +
       					 "Quisque facilisis erat a dui. Nam malesuada ornare dolor. Cras gravida, diam sit amet rhoncus ornare, erat elit consectetuer erat, id egestas pede nibh eget odio. Proin tincidunt, velit vel porta elementum, magna diam molestie sapien, non aliquet massa pede eu diam. Aliquam iaculis. Fusce et ipsum et nulla tristique facilisis. Donec eget sem sit amet ligula viverra gravida. Etiam vehicula urna vel turpis. Suspendisse sagittis ante a urna. Morbi a est quis orci consequat rutrum. Nullam egestas feugiat felis. Integer adipiscing semper ligula. Nunc molestie, nisl sit amet cursus convallis, sapien lectus pretium metus, vitae pretium enim wisi id lectus." +
      					  "Proin at eros non eros adipiscing mollis. Donec semper turpis sed diam. Sed consequat ligula nec tortor. Integer eget sem. Ut vitae enim eu est vehicula gravida. Morbi ipsum ipsum, porta nec, tempor id, auctor vitae, purus. Pellentesque neque. Nulla luctus erat vitae libero. Integer nec enim. Phasellus aliquam enim et tortor. Quisque aliquet, quam elementum condimentum feugiat, tellus odio consectetuer wisi, vel nonummy sem neque in elit. Curabitur eleifend wisi iaculis ipsum. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In non velit non ligula laoreet ultrices. Praesent ultricies facilisis nisl. Vivamus luctus elit sit amet mi.";				   
				    					 
				    }
				   	//-----------------
				      				    				            
					// ---------------- INIT
					$scope.init = function(){
						resetVariables();
						cfpLoadingBar.start();
						SmoothScroll.$goTo(0);							
						loadComponents(function(){
							sharedData.add("currentController", fileName);	// current controller
							start();
						});				
					};
					//-----------------
					
					//----------------- REFRESH
					function onRefresh(){
						sharedData.add("currentController", fileName);	// current controller
						$scope.masterData = sharedData.getAll();
						callController({who: "master", action: "hideThinking"});
						$scope.$apply();		
					};
					//-----------------							
					
					// ---------------- LOAD COMPONENTS
					function loadComponents(callback){
						checkMaster(function(state){
							if (state){
								$scope.masterData = sharedData.getAll();
								$scope.page.loadComponents[0] = true;
								animate(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										checkLoad(callback);
									}
								});									
								
							}
						});

						doSomething(function(state){
							if (state){
								$scope.page.loadComponents[2] = true;
								checkLoad(callback);
							}
						});						
					};
					//-------------------
					
					
					// ------------------ SAMPLE COMPONENTS
					function checkMaster(callback){
						// wait for master.js to finish loading 	
						sharedData.request("masterReady", function(state, data){
							if(state){
								if(data.ready == true){
									callback(true);
								}
								else{
									alert(data.ready);
								}
							}
							else{
								alert(data);
							}	
						});							

					}
					function animate(callback){
						// for mobile
						if ($scope.masterData.browserDetails.mobile){
							$('#' + fileName + '_id').addClass('gray-gradient');							
						}
						// for desktop
						else{
							custom.backstretchBG("media/background.jpg", 0, 1000);
						}		
						$timeout(function(){	
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 0}, 0)
								.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
								.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
								
							callback(true);
						}, 500); // slight delay needed
					}
					function doSomething(callback){
						callback(true);
					}
					//-----------------
					
					
					//----------------- CHECK LOAD
					function checkLoad(callback){
						var check = true,
							array = $scope.page.loadComponents; 					
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						// all loads completed
						if (check){
						// CONTENT PAGE IS READY - INSERT CODE HERE
							
								
							// check for existing modal
							var duration = 2000;
							var hasModalOpen = $('.reveal-modal').hasClass('in');
							if (!hasModalOpen){ duration = 0; };
							
							$timeout(function(){ 
								cfpLoadingBar.complete();
								$scope.page.isLoaded = true;																
								callController({who: "master", action: "hideSplash"});
								$scope.$apply();						
								callback();											
							}, duration);	
								
							
						}						
					};						
					//-----------------
//-------------------------------------		









			
					
//-------------------------------------
					//----------------- START 
					function start(){

						// CONTENT PAGE IS READY - INSERT CODE HERE
						//console.log($scope.masterData);	
						
							
					}
					//-----------------
					
					
					//-----------------
					$scope.testbtn1 = function(){
						
						var myArray = ['Option 1', 'Option 2', 'Option 3', 'Option 4'];	
						callController({who: "master", action: "choiceBox", data: myArray}, function(data){ 
							console.log(data);
						});	

					};
					//-----------------

					//-----------------
					$scope.testbtn2 = function(){
							
						callController({who: "master", action: "confirmBox"}, function(data){ 
							console.log(data); 
						});	
							

					};
					//-----------------
//-------------------------------------















//-------------------------------------					
					// ----------------  PING/PONG	  		
					$scope.pong = function(callback){
						callback({status: fileName, msg: "pong"});
					};
					// ---------------- 					
					
					// -----------------
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
	
					var runRequest = (function () {
						
					  var callback = function(data, returnData){
						var packet = {
								info:{
									to: data.info.from,
									from: fileName
								},
								execute: {
									name: "calledBack",
									callback: false
								},
								returnData: {
									data: returnData
								}
							};					
						$scope.broadcast(packet);
					  };						
						
					  var execute = function (data) {
							switch(data.execute.name) {
								
								 // required for callbacks to master controller
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;
							     // add to this list if you need to call this specific controller
							    case "refresh":
							        onRefresh();
							    break;								     
							    case "ping":
							        $scope.pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							   
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };		
					})();
					
					// shorthand for calling 
					var callController = function(e, returnPacket){
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 
							if (e.data == undefined || e.data == null){
								e.data = {};
							}
							
							var packet = {
									info:{
										to: e.who,
										from: fileName,
										data: e.data
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
								
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});						
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
						  var execute = function(packet, callback){
						  		packet["order"] = executeOrder.length;
						  		
						  		executeOrder.push(packet);
								$scope._watched = {execute: null, callback: null};
								var unbindWatch = $scope.$watch('_watched.execute', function() {	 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};
							      	
							    });
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// ----------------
//-------------------------------------				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
