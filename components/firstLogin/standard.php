<div id='content-page' class='initialHide' >
	<!-- PAGE NAME -->
	<div class="row" ng-if='page.isLoaded == false'>
		<div class="small-12 columns">
			<gap></gap>
			<gap></gap>
				<centered>
					<h3>
						Loading information.  Please wait.
					</h3>
				</centered>
			<gap></gap>
		</div>
	</div>
	
	<div class="row" ng-if='page.isLoaded == true'>
		<div class="small-12 columns">
				<gap></gap>
				<h1>FIRST LOGIN (DESKTOPS)</h1>				
				<hr>
				<div ng-if='page.status == null'>
					<centered>
						<h5>Error loading the information.</h5>
						<button onclick='location.reload()'>Reload Page</button>
					</centered>
				</div>				
				
				<div ng-if='page.status == "success"'>
					<div class='small-12 medium-6 large-6 columns'>
					<p>Please enter a new password.</p>
					<formly-form result="formData"
								 fields="formFields"
								 options="formOptions"
								 ng-submit="update()">
					</formly-form>		
					</div>				
					
				</div>
				
				<div ng-if='page.status == "fail"'>
					
					<div class='small-12 columns'>
						
			    	<h3>Login or Create an Account</h3>
			    	<centered>	    
						<button ng-click="first_createAccount()" class="button"><small>Create New Account</small></button>	
						<button ng-click="first_login()" class="button"><small>Login</small></button>
					</centered>						
						
						<br>
						<p>* Your temporary password has expired.  If you've forgotten your password, choose <em>"Reset my password"</em> above.</p>
					</div>	
				</div>		
				
				<div ng-if='page.status == "loggedIn"'>
					LOGIN already logged in
				</div>								
				
				
				
		</div>		
	</div>	
	
</div>