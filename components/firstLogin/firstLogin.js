define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	
	var fileName  = 'firstLogin';
  	
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
				app.controller(fileName + 'Controller', function($stateParams, $state, $rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar) {	   
				    
//-------------------------------------				    
				    // ---------------- VARIABLES 
				    function resetVariables(){ 
				    $scope.page = {
				    	loadComponents: custom.fillArray(3),  // number should equal # of load components below
				    	isLoaded: false,
				    	email: $stateParams.email,
						tempPassword: $stateParams.tempPassword,
						status: null,
						user: null
				    };
					
					$scope.formData = {
						password: null,
						confirm: null
					};
					
					$scope.formFields = [					
						{
							key: 'password',
							type: 'password',
							label: 'Password',
							placeholder: 'Password',
							required: true,
							min: 8
						},
						{
							key: 'confirm',
							type: 'password',
							label: 'Reenter Password',
							placeholder: 'Confirm',
							required: true,
							min: 8
						}
					];
					};
				   	//-----------------
				      				    				            
					// ---------------- INIT
					$scope.init = function(){
						resetVariables();
						cfpLoadingBar.start();
						SmoothScroll.$goTo(0);							
						loadComponents(function(){
							sharedData.add("currentController", fileName);	// current controller
							start();
						});				
					};
					//-----------------
					
					//----------------- REFRESH
					function onRefresh(){
						sharedData.add("currentController", fileName);	// current controller
						$scope.masterData = sharedData.getAll();
						callController({who: "master", action: "hideThinking"});
						$scope.$apply();		
					};
					//-----------------							
					
					// ---------------- LOAD COMPONENTS
					function loadComponents(callback){
						checkMaster(function(state){
							if (state){
								$scope.masterData = sharedData.getAll();
								$scope.page.loadComponents[0] = true;
								animate(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										checkLoad(callback);
									}
								});									
								
							}
						});

						doSomething(function(state){
							if (state){
								$scope.page.loadComponents[2] = true;
								checkLoad(callback);
							}
						});						
					};
					//-------------------
					
					
					// ------------------ SAMPLE COMPONENTS
					function checkMaster(callback){
						// wait for master.js to finish loading 	
						sharedData.request("masterReady", function(state, data){
							if(state){
								if(data.ready == true){
									callback(true);
								}
								else{
									alert(data.ready);
								}
							}
							else{
								alert(data);
							}	
						});							

					}
					function animate(callback){
						// for mobile
						if ($scope.masterData.browserDetails.mobile){
							$('#' + fileName + '_id').addClass('gray-gradient');							
						}
						// for desktop
						else{
							custom.backstretchBG("media/background.jpg", 0, 1000);
						}		
						$timeout(function(){	
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 0}, 0)
								.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
								.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
								
							callback(true);
						}, 500); // slight delay needed
					}
					function doSomething(callback){
						callback(true);
					}
					//-----------------
					
					
					//----------------- CHECK LOAD
					function checkLoad(callback){
						var check = true,
							array = $scope.page.loadComponents; 					
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						// all loads completed
						if (check){
						// CONTENT PAGE IS READY - INSERT CODE HERE
							cfpLoadingBar.complete();
								
							
							$timeout(function(){ 
																								
								callController({who: "master", action: "hideSplash"});
								$scope.$apply();						
								callback();											
							}, 2000);	
								
							
						}						
					};						
					//-----------------
//-------------------------------------		

					
					

					//----------------- START 
					function start(){

						// CONTENT PAGE IS READY - INSERT CODE HERE
						checkUser();
						
			
				
					}
					//-----------------
					
					
					//----------------- EVENTS
				    $scope.first_login = function(){
						callController({who: "master", action: "login"}, function(data){
							console.log(data)
						});
                    };
                    $scope.first_createAccount = function(){
						callController({who: "master", action: "createNewAccount"}, function(data){
							if(data.data == "updated"){ 
								alert("new account created");	
							}
						});	
                    };
                    //-----------------	
					
					
					
					//----------------- LOGIN
					function checkUser(){
						
						if ($scope.masterData.logState == false){
			 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );						
							var check = 0; checkPass = true; 
							var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {
										
									if (check > 0 && checkPass){ 	
										// FAILURE TO LOGIN
										if (error != null){
											setPageStatus("fail");	
											checkPass = false;
										}
										// LOGIN SUCCESSFUL
										else{
											$scope.page.user = user;
											setPageStatus("success");											
											checkPass = false;
										}
									}
									else{
										check++;
									}
									$scope.page.isLoaded = true;
	
									
								});		
								auth.login('password', {  // PASSWORD IS THE METHOD ()
								  email:		$scope.page.email,
								  password: 	$scope.page.tempPassword
								});		
							}
							else{								
								$state.go('home');						
							}
			  				
					 }	
					//-----------------

					//-----------------
					function setPageStatus(type){
						$timeout(function(){ 
							callController({who: "master", action: "hideSplash"});	
							$scope.page.status = type;
							$scope.$apply();										
						}, 0);							

					};
					//-----------------


					//-----------------
					$scope.update = function(){
						if ($scope.formData.password.length < 8){
							alert("Password must be at least 8 characters in length.");
						}
						else if ($scope.formData.password != $scope.formData.confirm){
							alert("Passwords do not match.  Please try again.");
						}
						else{
							callController({who: "master", action: "showThinking"});
							
							var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
							var auth = new FirebaseSimpleLogin(fbLogin, function(error, user) {		});								
							auth.changePassword($scope.page.email, $scope.page.tempPassword, $scope.formData.password, function(error, success) {							  
							  if (!error) {	
							  	alert("Your password has been updated.  Please login.");			   	
							    $state.go('home');			  								    
							  }
							  else{							  								  	
							  	alert(error);
							  	callController({who: "master", action: "hideThinking"});
							  }
							});	

						}
					};
					//-----------------








					
//-------------------------------------					
					// ----------------  PING/PONG	  		
					$scope.pong = function(callback){
						callback({status: fileName, msg: "pong"});
					};
					// ---------------- 					
					
					// -----------------
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
	
					var runRequest = (function () {
						
					  var callback = function(data, returnData){
						var packet = {
								info:{
									to: data.info.from,
									from: fileName
								},
								execute: {
									name: "calledBack",
									callback: false
								},
								returnData: {
									data: returnData
								}
							};					
						$scope.broadcast(packet);
					  };						
						
					  var execute = function (data) {
							switch(data.execute.name) {
								
								 // required for callbacks to master controller
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;
							     // add to this list if you need to call this specific controller
							    case "refresh":
							        onRefresh();
							    break;								     
							    case "ping":
							        $scope.pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							   
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };		
					})();
					
					// shorthand for calling 
					var callController = function(e, returnPacket){
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 
							if (e.data == undefined || e.data == null){
								e.data = {};
							}
							
							var packet = {
									info:{
										to: e.who,
										from: fileName,
										data: e.data
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
								
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});						
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
						  var execute = function(packet, callback){
						  		packet["order"] = executeOrder.length;
						  		
						  		executeOrder.push(packet);
								$scope._watched = {execute: null, callback: null};
								var unbindWatch = $scope.$watch('_watched.execute', function() {	 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};
							      	
							    });
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// ----------------
//-------------------------------------	
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
