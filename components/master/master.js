define(['custom', 'sharedData', 'firebaseSettings'], function(custom, sharedData, firebaseSettings) {

	
	var fileName  = 'master';
 
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
				app.controller(fileName + 'Controller', function(	$state, $stateParams, $rootScope, $scope, $timeout, $modal,  
																	$detection, $localStorage, $sessionStorage, 
																	SmoothScroll, angularLoad,  
																	browserInfo, cfpLoadingBar, browserInfo, 
																	accountModalCtrl, uiModalCtrl) {

				    // VARIABLES ------
				    function resetVariables(){
					    $scope.page = {
					    	loadComponents: custom.fillArray(8),				    	
					    	browserSettings: browserInfo.giveMeAllYouGot()
					    };
				    }
				   	$('body').css('opacity', 0);
				   	//-----------------
				      	      
					// ---------------- INIT
					$scope.init = function(){	
						console.groupCollapsed("Init Sequence")
						console.time("Loadtime")
						resetVariables();					
						loadComponents(function(){
							start();
						});																								
					};
					//-----------------
					
					//----------------- REFRESH
					$scope.masterRefresh = function(){	
						    
							console.groupCollapsed("Refresh Sequence")
							console.time("Loadtime")
								
							var currentPage = sharedData.getAll().currentController; 
							callController({who: "master", action: "showThinking"});
							
    						resetVariables();
   						    sharedData.clearAll();	
							loadComponents(function(){
								
	
															
								$timeout(function(){
									sharedData.add("masterReady", {ready: true});	
									
									// REFRESH CURRENT PAGE
									var packet = {
											info:{to: currentPage,	from: fileName},
											execute: { name: "refresh",	callback: false	}
										};					
									$scope.broadcast(packet);
									
									// REFRESH HEADER
									var packet = {
											info:{to: "header",	from: fileName},
											execute: { name: "refresh",	callback: false	}
										};					
									$scope.broadcast(packet);	
									
									// REFRESH FOOTER
									var packet = {
											info:{to: "footer",	from: fileName},
											execute: { name: "refresh",	callback: false	}
										};					
									$scope.broadcast(packet);	
									
									// REFRESH OVERLAY
									var packet = {
											info:{to: "offcanvas",	from: fileName},
											execute: { name: "refresh",	callback: false	}
										};					
									$scope.broadcast(packet);	
									
									// REFRESH OFF CANVAS
									var packet = {
											info:{to: "overlay",	from: fileName},
											execute: { name: "refresh",	callback: false	}
										};					
									$scope.broadcast(packet);
																		
									console.timeEnd("Loadtime");
									console.groupEnd("Refresh Sequence")
																
								}, 2000);
									
							});
					};
					//-----------------
							
					//----------------- LOAD COMPONENTS 
					function loadComponents(_callback){
						console.log("initalizeDatabase started...")					
						initalizeDatabase(function(state){								
							if (state){
								console.log("initalizeDatabase complete.")		
								if ($scope.page.loadComponents[0] != true){
								  $scope.page.loadComponents[0] = true;
								  checkLoad(_callback);
								}
							}
						});
						
						console.log("fetchUrl started...")	
						fetchUrl(function(state){														
							if (state){
								console.log("fetchUrl complete.")	
								if ($scope.page.loadComponents[1] != true){
							  	  $scope.page.loadComponents[1] = true;
								  checkLoad(_callback);
								}
							}
						});
						
						console.log("fetchIPAddress started...")	
						fetchIPAddress(function(state){							
							if (state){
								console.log("fetchIPAddress complete.")	
								if ($scope.page.loadComponents[2] != true){
								  $scope.page.loadComponents[2] = true;
								  checkLoad(_callback);
								}
							}
						});	
						
						console.log("detectType started...")		
						detectType(function(state){												
							if (state){
								console.log("detectType complete.")
								if ($scope.page.loadComponents[3] != true){
								  $scope.page.loadComponents[3] = true;
								  checkLoad(_callback);
								}
							}
						});	
						
						console.log("userData started...")											
						userData(function(state){
							if (state){
								console.log("userData complete.")
								if ($scope.page.loadComponents[4] != true){
								  $scope.page.loadComponents[4] = true;
								  checkLoad(_callback);
								}
							}
						});
						
						console.log("userDetails started...")	
						userDetails(function(state){											
							if (state){
								if ($scope.page.loadComponents[5] != true){
								  $scope.page.loadComponents[5] = true;
								  checkLoad(_callback);
								}
							}
						});	
						
						console.log("getBrowsrDetails started...")
						getBrowserDetails(function(state){														
							if (state){
								console.log("getBrowsrDetails complete.")
								if ($scope.page.loadComponents[6] != true){
								  $scope.page.loadComponents[6] = true;
								  checkLoad(_callback); 
								}
							}
						});	
						
						console.log("getProperCSS started...")
						getProperCSS(function(state){
							
							if (state){
								console.log("getProperCSS complete.")
								if ($scope.page.loadComponents[7] != true){
								  $scope.page.loadComponents[7] = true;
								  checkLoad(_callback); 	
								}
							}
						});	
							
					};
					//-------------------
					
					
					// LOAD COMPONENTS
					function initalizeDatabase(callback){
						
						
						custom.databaseInit(function(status){
							if (status){								
								callback(true);
							}
							else{
								alert("Timeout: Database could not be reached.");
							}
						});
						
					};
					
					function fetchUrl(callback){
						phpJS.getFullUrl(function(state, data){
							if (state){
								sharedData.add('url', JSON.parse(data) );
								callback(true);
							}else{
								alert("Timeout:  Could not fetch URL.");
							}
						});
					};
					
					function fetchIPAddress(callback){					
						phpJS.getIPAddress(function(state, data){
							if (state){
								sharedData.add('ipAddress', JSON.parse(data) );
								callback(true);
							}else{
								alert("Timeout: IP Address could not be retrieved.");
							}	
						});						
					};
					
					function detectType(callback){
					  	var type = '';
					  	if ($detection.isAndroid()){
					  		type = "android";
					  	}
					  	else if($detection.isiOS()){				  		
					  		type = "ios";
					  	}
					  	else if($detection.isWindowsPhone()){
					  		type = "windows-phone";
					  	}
					  	else{				  		
					  		type = "desktop";
					  	}
						sharedData.add('ios', type);					   											
						callback(true);						
					};
					
					function userData(callback){
						
			  			firebaseSettings.getPermissionLevel(function(isUser, details){			  				
			  				var accessLevel = {
			  					isUser: isUser,
			  					permission:  details.type,
			  					value: details.value
			  				};
			  				sharedData.add("accessLevel", accessLevel);
			  				callback(true);					  				
			  			});	
			  			
					};
					
					function userDetails(callback){
				  		var userData = [];	

					  		firebaseSettings.checkUserData(function(returnState, data){					  			
								if(returnState){
									// retrieved user data
									userData = data.user;	
									userData["avatar"] = data.image;
									userData["auth"] = JSON.parse(localStorage.getItem("user")).auth; 
									sharedData.add("logState", true);	
									localStorage.setItem("logState", true);
																									
								}
								else{
									// not logged in
									userData = {
										id: null,
										email: null,
										firstName: null,
										lastname: null,
										permission: 'guest',										
										userName: 'anonymous',
									};
									userData["avatar"] = {small: null, standard: null, thumbnail: null};
									userData["auth"] = null;
									sharedData.add("logState", false);
									localStorage.setItem("logState", false);
								};
								sharedData.add("userInfo", userData);
								callback(true);								
							});	
					
	
											
								
					};
					
					function getBrowserDetails(callback){
						 var size = null,
						 	 width = browserInfo.giveMeAllYouGot().screenSize.width; 
						 
						 if (width < 550){
						 	size = "small";
						 }
						 if (width >= 550 && width < 1080){						
						 	size = "medium";
						 }
						 if (width >= 1080){						
						 	size = "large";
						 }		
						 sharedData.add('browserSize', size);				 
						 sharedData.add('browserDetails', browserInfo.giveMeAllYouGot() );
						 callback(true);
					};
					
					function getProperCSS(callback){
						
						sharedData.request('browserDetails', function(state, data){
							if (state){
								var isMobile = data.mobile;
								
								if(isMobile){
							    	var custom_css = 'css/custom-mobile.css';
							   	}
							   	else{
							   		var custom_css = 'css/custom-standard.css';
							   	}
					   	
							  	angularLoad.loadCSS(custom_css)
							  			.then(function() {
										   callback(true);
										}).catch(function() {
										   callback(false);
										});	


							}
							else{
								alert("Error in getProperCSS.")
							}
						});
						
					};
					//-----------------
					
					
					// CHECK LOAD -----
					function checkLoad(callback){
						var check = true,
							array = $scope.page.loadComponents; 	
						
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						
						// all loads completed
						if (check){
							callback();
						}						
					};						
					// -----------------



					// START ----------
					function start(){							
						$timeout(function(){
							console.timeEnd("Loadtime");
							console.groupEnd("Init Sequence");
							
							sharedData.add("masterReady", {ready: true});
							callController({who: "master", action: "showSplash"}, function(data){});
							$('body').transition({   opacity: 1}, 2000).css('background-color', 'transparent');	
						}, 500);
					}
					//-----------------
					
					
					
					
					
					
					// ----------------  PING/PONG	  		
					$scope.__pong = function(callback){
						callback({status: fileName, msg: "pong"});
					};
					// ---------------- 
					
					// ----------------  BROWSER INFO		  		
					$scope.__browserInfo = function (callback) {
							;
							var control = uiModalCtrl.browserInfoCtrl(); 
							var modalInstance = $modal.open({
						      	templateUrl: 'browserInfoModal.html',
						  		controller: control,
							    resolve: {
							        data: function () {
							       
							        }
							    }
							});
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE																											
									;
									callback({status: "closed", msg: "closed"});	
									}, 
								function () {			// DISMISS	
									
						  			callback({status: "dismiss", data: "dismissed"});	
						    	}
						    );						
					};

					// ----------------  CONFIRM		  					  
					$scope.__choiceBox = function (data, callback) {
						  
						  $scope.items = data;
  
						  $scope.open = function () {
						    var modalInstance = $modal.open({
						      templateUrl: 'choiceModal.html',
						      controller: uiModalCtrl.choiceModalCtrl,
						      resolve: {
						        items: function () {
						          return $scope.items;
						        }
						      }
						    });

							// VIA CLOSE
							modalInstance.result.then(function(returnData)
							{			
								callback({status: "closed", data: returnData});					  							 		
							}, 
							// VIA DISMISS
							function (){
								
						  		callback({status: "dimiss", data: "dismissed"});
							});	
						  };

						$scope.open();

					};
					// ---------------- 
					
					
					// ----------------  CONFIRM		  					  
					$scope.__confirmBox = function (callback) {

						  $scope.open = function () {
						    var modalInstance = $modal.open({
						      templateUrl: 'confirmModal.html',
						      controller: uiModalCtrl.confirmModalCtrl,
						      resolve: {
						        
						      }
						    });

							// VIA CLOSE
							modalInstance.result.then(function(returnData)
							{			
								callback({status: "closed", data: returnData});					  							 		
							}, 
							// VIA DISMISS
							function (){
								
						  		callback({status: "dimiss", data: "dismissed"});
							});	
						  };

						$scope.open();

					};
					// ---------------- 					


				 	// ---------------- LOGOUT
					$scope.__logout = function (callback) {						
		 				
		 				var checkOnce = false; 
		 				var fbLogin = new Firebase( firebaseSettings.firebaseRoot() );
						var auth = new FirebaseSimpleLogin(fbLogin, function() {
								if (!checkOnce){
									checkOnce = true; 			
									firebaseSettings.setLogout();								
					 			} 	
						});	
		 				auth.logout();	
					};
					//-----------------

				 	// ---------------- RESET PASSWORD
					$scope.__resetPassword = function (callback) {						
						
						var modalInstance = $modal.open({
					      	templateUrl: 'resetModal.html',
					  		controller: accountModalCtrl.resetEmailCtrl(),
						});
						// VIA CLOSE
						modalInstance.result.then(function(returnData)
						{							 
							
							callback({status: "closed", data: "updated"});					  							 		
						}, 
						// VIA DISMISS
						function (){
							
					  		callback({status: "dimiss", data: "dismissed"});
						});	
						
					};
					//-----------------

				 	// ---------------- LOGIN
					$scope.__login = function (callback) {						
						
						var modalInstance = $modal.open({
					      	templateUrl: 'loginModal.html',
					  		controller: accountModalCtrl.loginModalCtrl(),
						});
						// VIA CLOSE
						modalInstance.result.then(function(returnData)
						{							 
							

							callback({status: "closed", data: "closed"});					  							 		
						}, 
						// VIA DISMISS
						function (){
							
					  		callback({status: "dimiss", data: "dismissed"});
						});	
						
					};
					//-----------------
					
					
					//----------------- EDIT PROFILE IMAGE					
					$scope.__editProfilePic = function(callback){						
											
							
							
							var modalInstance = $modal.open({
						      	templateUrl: 'editProfileModal.html',
						  		controller:  accountModalCtrl.editProfileModalCntrl(),
							    resolve: {
							        data: function () {
							        // return $scope.deleteUserObj;
							        }
							    }
							});
						
						
							// VIA CLOSE
							modalInstance.result.then(function(returnData)
							{		
								
								callback({status: "closed", data: "updated"});					  							 		
							}, 
							// VIA DISMISS
							function (){
								
						  		callback({status: "dimiss", data: "dismissed"});
							});	
					
		
					};
					//-----------------		
					
					
					//-----------------	EDIT PROFILE IMAGE					
					$scope.__editProfileData = function(callback){						
												
							
							
							var modalInstance = $modal.open({
						      	templateUrl: 'editProfileDataModal.html',
						  		controller: accountModalCtrl.editProfileDataModalCntrl(),
							    resolve: {
							        data: function () {
							        	
							        // return $scope.deleteUserObj;
							        }
							    }
							});
						
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE																														
									
									callback({status: "closed", data: "updated"});	
									}, 
								function () {			// DISMISS
									
									
						  			callback({status: "closed", data: "dismissed"});	
						    });
					
		
					};
					//-----------------								
					
					//----------------- CHANGE PASSWORD
					$scope.__changePassword = function(callback){
							
							
							var modalInstance = $modal.open({
						      	templateUrl: 'changePasswordModal.html',
						  		controller: accountModalCtrl.changePasswordCtrl(),
							    resolve: {
							        data: function () {
							        // return $scope.deleteUserObj;
							        }
							    }
							});
							
							modalInstance.result.then(
								function (returnData) {  // CLOSE																														
									
									callback({status: "closed", data: "updated"});	
									}, 
								function () {			// DISMISS
									
						  			callback({status: "closed", data: "dismissed"});	
						    });
					};
					//-----------------	
					
					//-----------------	DELETE ACCOUNT
					$scope.__deleteMyAccount = function(callback){
										
						
						var modalInstance = $modal.open({
					      	templateUrl: 'deleteModal.html',
					  		controller: accountModalCtrl.deleteModalCtrl(),
						    resolve: {
						        data: function () {
						         // return $scope.deleteUserObj;
						        }
						    }
						});
					
						modalInstance.result.then(
							function (returnData) {  // CLOSE																														
								
								callback({status: "closed", data: "deleted"});	
								}, 
							function () {			// DISMISS
								
					  			callback({status: "closed", data: "dismissed"});	
					    });
					};	
					//-----------------				
					
					//-----------------	 CREATE NEW ACCOUNT
					$scope.__createNewAccount = function(callback){
							
							
							
							var modalInstance = $modal.open({
						      	templateUrl: 'createModal.html',
						  		controller: accountModalCtrl.createModalCtrl(),
							    resolve: {
							        data: function () {
							        // return $scope.deleteUserObj;
							        }
							    }
							});
						
						
							modalInstance.result.then(
								function (returnData) {  // CLOSE
									
									callback({status: "closed", data: "created"});	
									
									}, 
								function () {			// DISMISS
									
						  			callback({status: "closed", data: "dismissed"});	
						    });
					
		
					};
					//////////////////////					
								
					
					//----------------- RELOAD DOM
					$scope.__refreshPage = function(){						
						$state.transitionTo($state.current, $stateParams, {
							    reload: true,
							    inherit: false,
							    notify: true
						});								
					};
					//-----------------
					
					//----------------- REFRESH DATA
					$scope.__refreshData = function(){
						$scope.masterRefresh();
					};
					//-----------------

					//----------------- SHOW/HIDE THINKING
					$scope.__showSplash = function(callback){
						var isActive = $(".reveal-modal").hasClass("in"); 
						if (!isActive){
													
							var modalInstance = $modal.open({
						      	templateUrl: 'splashModal.html',
						  		controller: uiModalCtrl.splashModalCtrl(),
							});
							
							
							callback({active: isActive});
						};		
						callback({active: isActive});											
					};
					$scope.__hideSplash = function(callback){
					
						$(".reveal-modal").transition({ top: '-50%', opacity: 0 }, 500);
						$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
						var isActive = $(".reveal-modal").hasClass("in"); 
						
						if (isActive){
							setTimeout(function(){
								$('.modal-body').addClass('hidden');
								$(".reveal-modal").removeClass();
								$(".reveal-modal-bg").removeClass();
								$('body').removeClass('modal-open');
								callback({active: isActive});
							}, 500);	
						}
						callback({active: isActive});
						
					};
					//-----------------	

					//----------------- SHOW/HIDE THINKING
					$scope.__showThinking = function(callback){
						var isActive = $(".reveal-modal").hasClass("in"); 
						if (!isActive){
													
							var modalInstance = $modal.open({
						      	templateUrl: 'thinkingModal.html',
						  		controller: uiModalCtrl.thinkingModalCtrl(),
							});
							callback({active: isActive});
						};	
						callback({active: isActive});												
					};
					$scope.__hideThinking = function(callback){
						$(".reveal-modal").transition({ top: '-50%', opacity: 0 }, 500);
						$(".reveal-modal-bg").transition({ opacity: 0 }, 500);
						var isActive = $(".reveal-modal").hasClass("in"); 
						
						if (isActive){
							setTimeout(function(){
								$('.modal-body').addClass('hidden');
								$(".reveal-modal").removeClass();
								$(".reveal-modal-bg").removeClass();
								$('body').removeClass('modal-open');
								callback({active: isActive});
							}, 500);	
						}
						callback({active: isActive});
					};
					//-----------------					
					
					
					
					
					
					
					
					
					
					// -----------------
					// COMMUNICATE BETWEEN CONTROLLERS	 				  
					$scope.$on(fileName + '_recieve', function(e, data) { 
						runRequest.execute(data);
					});

					$scope.broadcast = function(packet){
						$rootScope.$broadcast(packet.info.to + "_recieve", packet );
					};


					var runRequest = (function () {
					
					  var callback = function(data, returnData){
						var packet = {
								info:{
									to: data.info.from,
									from: fileName
								},
								execute: {
									name: "calledBack",
									callback: false
								},
								returnData: {
									data: returnData
								}
							};					
						$scope.broadcast(packet);
					  };
					
					  var execute = function (data) {
					  		
					  	
							switch(data.execute.name) {
								 // required for callbacks to other controllers
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;								
							    case "ping":
							        $scope.__pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								
							    case "login":
							        $scope.__login(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "logout":
							        $scope.__logout(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "resetPassword":
							        $scope.__resetPassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;				    
							    case "editProfileImage":
							        $scope.__editProfilePic(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "createNewAccount":
							        $scope.__createNewAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "editProfileData":
							        $scope.__editProfileData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "deleteMyAccount":
							        $scope.__deleteMyAccount(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    
							     
							    case "changePassword":
							        $scope.__changePassword(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	 							    						    
							    case "confirmBox":
							        $scope.__confirmBox(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "choiceBox":
							        $scope.__choiceBox(data.info.data, function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    
							    case "browserInfo":
							        $scope.__browserInfo(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;
							    case "showSplash":
							        $scope.__showSplash(function(returnData){							        	
							        	if (data.execute.useCallback){							        		
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideSplash":
							        $scope.__hideSplash(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    	
							    case "showThinking":
							        $scope.__showThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							    case "hideThinking":
							        $scope.__hideThinking(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    							    							    
							    case "refreshPage":
							        $scope.__refreshPage(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;								    						    
							    case "refreshData":
							        $scope.__refreshData(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;							    
							    
							    default:
							      // do something
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };
					
					})();


					// shorthand for calling 
					var callController = function(e, returnPacket){
						
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 

							var packet = {
									info:{
										to: e.who,
										from: fileName
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
							
							
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});		
								
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
							
						
						  var execute = function(packet, callback){
								$scope._watched = {execute: null, callback: null};								
								var unbindWatch = $scope.$watch('_watched.execute', function() { 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};	
							    });
						  		packet["order"] = executeOrder.length;						  		
						  		executeOrder.push(packet);							    
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// -----------------			
					
					
				   

				});		
	    },
	    ///////////////////////////////////////
  };
});
