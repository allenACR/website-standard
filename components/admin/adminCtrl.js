define(['custom', 'sharedData'], function(custom, sharedData) {

	
	var fileName  = 'admin';
  	
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			
				app.controller(fileName + 'Controller', function($rootScope, $scope, $timeout, SmoothScroll, cfpLoadingBar, toaster) {	    


//-------------------------------------
				    // ---------------- VARIABLES 
				    function resetVariables(){
				    	$scope.page = {
				    		loadComponents: custom.fillArray(3),  // number should equal # of load components below
				    		isLoaded: false,
				    	};
				    }
				   
				   	//-----------------
				      				    				            
					// ---------------- INIT
					$scope.init = function(){
						resetVariables();
						sharedData.add("currentController", fileName);	// current controller
						cfpLoadingBar.start();
						SmoothScroll.$goTo(0);		
						loadComponents(function(){
							start();
						});				
					};
					//-----------------
					
					//----------------- REFRESH
					function onRefresh(callback){
						sharedData.add("currentController", fileName);	// current controller
						$scope.masterData = sharedData.getAll();
						
						callController({who: "master", action: "hideThinking"});
						$scope.$apply();		
					};
					//-----------------							
									
					
					// ---------------- LOAD COMPONENTS
					function loadComponents(callback){
						checkMaster(function(state){
							if (state){
								$scope.masterData = sharedData.getAll();
								$scope.page.loadComponents[0] = true;
								animate(function(state){
									if (state){
										$scope.page.loadComponents[1] = true;
										checkLoad(callback);
									}
								});	
							}
						});

						doSomething(function(state){
							if (state){
								$scope.page.loadComponents[2] = true;
								checkLoad(callback);
							}
						});						
					};
					//-------------------
					
					
					// ------------------ SAMPLE COMPONENTS
					function checkMaster(callback){
						// wait for master.js to finish loading 		
						sharedData.request("masterReady", function(state, data){
							if(state){
								if(data.ready == true){
									callback(true);
								}
								else{
									alert(data.ready);
								}
							}
							else{
								alert(data);
							}	
						});							

					}
					function animate(callback){
						// for mobile
						if ($scope.masterData.browserDetails.mobile){
							$('#' + fileName + '_id').addClass('gray-gradient');		
						}
						// for desktop
						else{
							custom.backstretchBG("media/background.jpg", 0, 1000);
						}		
						$timeout(function(){	
							$('#content-page')
								.transition({   x: -20,  opacity: 0, delay: 0}, 0)
								.transition({   x: 0,    opacity: 1, height: "auto", delay: 0}, 500)
								.css({"min-height": $scope.masterData.browserDetails.windowSize.height});
								
							callback(true);
						}, 500); // slight delay needed
					}
					function doSomething(callback){
						callback(true);
					}
					//-----------------
					
					
					//----------------- CHECK LOAD
					function checkLoad(callback){
						var check = true,
							array = $scope.page.loadComponents; 					
						var i = array.length; while(i--){
							if ( array[i] == false){
								check = false; 
							};
						};
						// all loads completed
						if (check){

							// check for existing modal
							var duration = 2000;
							var hasModalOpen = $('.reveal-modal').hasClass('in');
							if (!hasModalOpen){ duration = 0; };
							
							$timeout(function(){ 
								cfpLoadingBar.complete();
								$scope.page.isLoaded = true;																
								callController({who: "master", action: "hideSplash"});
								$scope.$apply();						
								callback();											
							}, duration);		
								
						
						}						
					};						
					//-----------------
//-------------------------------------











					
					
//-------------------------------------
					//----------------- START 
					function start(){
						
						// CONTENT PAGE IS READY - INSERT CODE HERE
						console.log($scope.masterData);		
						
					}
					//-----------------
//-------------------------------------












					
//-------------------------------------					
					//----------------- EDIT ACCOUNT
					$scope.editAccount = function(){
						callController({who: "master", action: "editProfileData"}, function(data){

							if(data.data == "updated"){ 
								$timeout(function(){
								callController({who: "master", action: "showThinking"});
								}, 300);
								$timeout(function(){
									callController({who: "master", action: "refreshData"});
								}, 600);
							}
							
						});							
					};
					//-----------------		

					//----------------- EDIT ACCOUNT
					$scope.editPicture = function(){
						callController({who: "master", action: "editProfileImage"}, function(data){ 
							if(data.data == "updated"){ 
								$timeout(function(){
								callController({who: "master", action: "showThinking"});
								}, 300);
								$timeout(function(){
									callController({who: "master", action: "refreshData"});
								}, 600);
							}
						});							
					};
					//-----------------		
					
					//----------------- CHANGE PASSWORD
					$scope.changePassword = function(){
						callController({who: "master", action: "changePassword"}, function(data){				
							if(data.data == "updated"){ 
								toaster.pop('success', "Password has been updated!", '');
							}							
						});							
					};
					//-----------------	
					
					//----------------- RESET PASSWORD
					$scope.resetPassword = function(){
						callController({who: "master", action: "resetPassword"}, function(data){
								
							if(data.data == "updated"){ 
								toaster.pop('success', "Password has been reset!", 'Check your email for a new temporary password.');
							}							
						});							
					};
					//-----------------	
					
					//----------------- new ACCOUNT
					$scope.createNewAccount = function(){
						callController({who: "master", action: "createNewAccount"}, function(data){
							if(data.data == "updated"){ 
								toaster.pop('success', "Account has been created!", 'Check your email for a login link.');							
							}							
						});	
					};
					//-----------------	
					
					//----------------- new ACCOUNT
					$scope.deleteMyAccount = function(){
						callController({who: "master", action: "deleteMyAccount"}, function(data){
							if(data.data == "deleted"){ 
								alert("Your account has been deleted.");
								localStorage.clear();
								location.reload();	
							}
						});	
					};
					//-----------------						
					
					//----------------- LOGIN
					$scope.adminLogin = function(){
						
						callController({who: "master", action: "login"}, function(data){
							if(data.data == "updated"){ 
								$timeout(function(){
									callController({who: "master", action: "showThinking"});
								}, 300);
								$timeout(function(){
									callController({who: "master", action: "refreshData"});
								}, 600);
							}
							
						});
						
					};
					//-----------------	
//-------------------------------------		







//-------------------------------------					
					// ----------------  PING/PONG	  		
					$scope.pong = function(callback){
						callback({status: fileName, msg: "pong"});
					};
					// ---------------- 					
					
					// -----------------
					// COMMUNICATE BETWEEN CONTROLLERS	  				
					$scope.$on(fileName + '_recieve', function(e, data) { runRequest.execute(data);	});
					$scope.broadcast = function(packet){ $rootScope.$broadcast(packet.info.to + "_recieve", packet );};
	
					var runRequest = (function () {
						
					  var callback = function(data, returnData){
						var packet = {
								info:{
									to: data.info.from,
									from: fileName
								},
								execute: {
									name: "calledBack",
									callback: false
								},
								returnData: {
									data: returnData
								}
							};					
						$scope.broadcast(packet);
					  };						
						
					  var execute = function (data) {
							switch(data.execute.name) {
								
								 // required for callbacks to master controller
							    case "calledBack":
							        $scope._watched.callback = data.returnData.data;
							    break;
							     // add to this list if you need to call this specific controller
							    case "refresh":
							        onRefresh();
							    break;								     
							    case "ping":
							        $scope.pong(function(returnData){
							        	if (data.execute.useCallback){
							        		callback(data, returnData); 
							        	};
							        });
							    break;	
							   
							}					   	
					  };
					    
					  return {
					    execute: execute
					  };		
					})();
					
					// shorthand for calling 
					var callController = function(e, returnPacket){
							var useCallback = true;
							if (returnPacket == null || returnPacket == undefined){
								useCallback = false;
							} 
							if (e.data == undefined || e.data == null){
								e.data = {};
							}
							
							var packet = {
									info:{
										to: e.who,
										from: fileName,
										data: e.data
									},
									execute: {
										name: e.action,
										useCallback: useCallback,
										callback: returnPacket
									}
								};		
								
							masterCall.execute(packet, function(data){
								if (returnPacket != null && returnPacket != undefined){
									returnPacket(data);	
								};			
							});						
					};
					
					// create callback system for talking to the master controller
					var executeOrder = [];
					var masterCall = (function () {
						  var execute = function(packet, callback){
						  		packet["order"] = executeOrder.length;
						  		
						  		executeOrder.push(packet);
								$scope._watched = {execute: null, callback: null};
								var unbindWatch = $scope.$watch('_watched.execute', function() {	 
										if($scope._watched.execute != null){
							       			$scope.broadcast($scope._watched.execute);	
							       		};
							      	
							    });
								$scope.$watch('_watched.callback', function() {										
										if($scope._watched.callback != null && $scope._watched.execute != null){
											$scope._watched.execute.execute.callback($scope._watched.callback);
											$scope._watched = {execute: null, callback: null}; 									
										}    	
										unbindWatch(); // remove event binder so it does not duplicate
							    });	
							   	$scope._watched.execute = packet;						   	
						  };
					    
						  return {
						    execute: execute
						  };		
					})();					
				    // 
					// ----------------
//-------------------------------------			    
				    
				   

				});
						
	    },
	    ///////////////////////////////////////
  };
});
