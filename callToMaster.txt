CALLS TO MASTER CHEATSHEAT
callController({who: "location", action: "ping"}, function(data){
	console.log(data)  // should return a pong if the controller is active
});

// choice box
var myArray = ['Option 1', 'Option 2', 'Option 3', 'Option 4'];	
callController({who: "master", action: "choiceBox", data: myArray}, function(data){ 
	console.log(data);
});	


// confirm box
callController({who: "master", action: "confirmBox"}, function(data){ 
	console.log(data); 
});	

// show/hide splash screen
callController({who: "master", action: "showSplash"});
callController({who: "master", action: "hideSplash"});	

// show/hide thinking box
callController({who: "master", action: "showThinking"});
callController({who: "master", action: "hideThinking"});


// display browser info in modal
callController({who: "master", action: "browserInfo"}, function(data){
					console.log(data);
});


// login
callController({who: "master", action: "login"}, function(data){
	if(data.data == "updated"){ 
		callController({who: "master", action: "showThinking"});
		}, 300);
		$timeout(function(){
			callController({who: "master", action: "refreshData"});
		}, 600);
	}
	
});

// logout
callController({who: "master", action: "login"}, function(data){
	console.log(data)
});


// reset password
callController({who: "master", action: "resetPassword"}, function(data){

	if(data.data == "updated"){ 
		alert("password changed");
		localStorage.clear();
		location.reload();	
	}
	
});	


// change password
callController({who: "master", action: "changePassword"}, function(data){

	if(data.data == "updated"){ 
		alert("password changed");
		localStorage.clear();
		location.reload();	
	}
	
});	

// create account
callController({who: "master", action: "createNewAccount"}, function(data){
	if(data.data == "updated"){ 
		alert("new account created");	
	}
});	

// delete account
callController({who: "master", action: "deleteMyAccount"}, function(data){
	if(data.data == "deleted"){ 
		alert("account has been deleted");
		localStorage.clear();
		location.reload();	
	}
});	

// edit and update profile information
callController({who: "master", action: "editProfileData"}, function(data){
	if(data.data == "updated"){ 
		$timeout(function(){
		callController({who: "master", action: "showThinking"});
		}, 300);
		$timeout(function(){
			callController({who: "master", action: "refreshData"});
		}, 600);
	}
});	
	
						    						    
// edit and update profile image
callController({who: "master", action: "editProfileImage"}, function(data){ 
	if(data.data == "updated"){ 
		$timeout(function(){
		callController({who: "master", action: "showThinking"});
		}, 300);
		$timeout(function(){
			callController({who: "master", action: "refreshData"});
		}, 600);
	}
});	
	